'use strict';
/**
 * Module Dependencies
 */
const winston = require('winston');
const WinstonSNS = require('winston-sns');
require('winston-logstash');
const path = require('path');
let config = {};
if (!process.env.LOGGER_FILE_LEVEL) {
    try {
        if (process.env.CONFIG_LOGGER_PATH) {
            config = require(path.join(process.cwd(), process.env.CONFIG_LOGGER_PATH));
        } else {
            config = require(path.join(process.cwd(), 'logger-config.js'));
        }
    } catch (e) {
        throw new Error('If not declare enviroment values, logger requires a logger-config.js file', e);
    }
}

winston.emitErrs = true;
/**
 * Custom Levels for the Winston Logger with colors of these loggers
 * @type {{levels: {fatal: number, error: number, warn: number, info: number, debug: number},
 * colors: {fatal: string, error: string, warn: string, info: string, debug: string}}}
 */
const levelsCustom = {
    levels: {
        fatal: 0,
        error: 1,
        warn: 2,
        info: 3,
        debug: 4
    },
    colors: {
        fatal: 'red',
        error: 'magenta',
        warn: 'grey',
        info: 'green',
        debug: 'blue'
    }
};
const transportsProviders = [];
if (process.env.LOGGER_FILE || config.hasOwnProperty('file')) {
    transportsProviders.push(new winston.transports.File({
        level: process.env.LOGGER_FILE_LEVEL || config.file.level,
        filename: process.env.LOGGER_FILE_PATH || config.file.path,
        handleExceptions: true,
        json: false,
        maxsize: process.env.LOGGER_FILE_MAX_SIZE || config.file.maxSize, //5242880, //5MB
        maxFiles: process.env.LOGGER_FILE_MAX_FILES || config.file.maxFiles,
        colorize: false
    }));
}
if (process.env.LOGGER_CONSOLE || config.hasOwnProperty('console')) {
    transportsProviders.push(new winston.transports.Console({
        level: process.env.LOGGER_CONSOLE_LEVEL || config.console.level,
        handleExceptions: true,
        silent: false,
        prettyPrint: true,
        json: false,
        colorize: true
    }));
}
if (process.env.LOGGER_SNS || config.hasOwnProperty('sns')) {
    transportsProviders.push(new WinstonSNS({
        level: process.env.LOGGER_SNS_LEVEL || config.sns.level,
        subscriber: process.env.LOGGER_SNS_SUBSCRIBER || config.sns.subscriber,
        topic_arn: process.env.LOGGER_SNS_TOPIC_ARN || config.sns.topic_arn,
        aws_key: process.env.AWS_KEY || config.sns.aws_key,
        aws_secret: process.env.AWS_SECRET || config.sns.aws_secret,
        region: process.env.AWS_REGION || config.sns.aws_region
    }));
}

if (process.env.LOGGER_LOGSTASH || config.hasOwnProperty('logstash')) {
    transportsProviders.push(new winston.transports.Logstash({
        level: process.env.LOGGER_LOGSTASH_LEVEL || config.logstash.level,
        port: process.env.LOGGER_LOGSTASH_PORT || config.logstash.port,
        node_name: process.env.LOGGER_LOGSTASH_NODE_NAME || config.logstash.nodeName,
        host: process.env.LOGGER_LOGSTASH_HOST || config.logstash.host,
        json: true
    }));
}

/**
 * Logger for winston.
 */
const logger = new winston.Logger({
    levels: levelsCustom.levels,
    transports: transportsProviders,
    exitOnError: false
});
/**
 * Winston override the colors with custom colors.
 */
winston.addColors(levelsCustom.colors);

class LoggerObject {
    /**
     * Constructor for the new Logger
     * @param {Object} options Object that represents a prorperties for the new logger
     * @param {String} options.project REQUIRED Project that represents logger
     * @param {String} options.file REQUIRED File of that represents file
     */
    constructor (options) {
        this.project = options.project;
        this.file = options.file;
    }

    /**
     * Function that inject the properties of logger to winton Logger
     * @param {Object} meta Object to send to logger
     * @private
     */
    _parseMeta (meta) {
        meta.project = meta.hasOwnProperty('project') ? meta.project : this.project;
        meta.file = meta.hasOwnProperty('file') ? meta.file : this.file;
        return meta;
    }

    /**
     * Logger Debug function
     * @param {String} textLog message
     * @param {Object} meta Meta information to send to logger.
     */
    debug (textLog, meta = null) {
        if (!meta) {
            meta = {};
        }
        meta = this._parseMeta(meta);
        logger.debug(textLog, meta);
    }

    /**
     * Logger Info function
     * @param {String} textLog message
     * @param {Object} meta Meta information to send to logger.
     */
    info (textLog, meta=null) {
        if (!meta) {
            meta = {};
        }
        meta = this._parseMeta(meta);
        logger.info(textLog, meta);
    }

    /**
     * Logger Warn function
     * @param {String} textLog message
     * @param {Object} meta Meta information to send to logger.
     */
    warn (textLog, meta=null) {
        if (!meta) {
            meta = {};
        }
        meta = this._parseMeta(meta);
        logger.warn(textLog, meta);
    }

    /**
     * Logger Error function
     * @param {String} textLog message
     * @param {Object} meta Meta information to send to logger.
     */
    error (textLog, meta=null) {
        if (!meta) {
            meta = {};
        }
        meta = this._parseMeta(meta);
        logger.error(textLog, meta);
    }

    /**
     * Logger Fatal function
     * @param {String} textLog message
     * @param {Object} meta Meta information to send to logger.
     */
    fatal (textLog, meta=null) {
        if (!meta) {
            meta = {};
        }
        meta = this._parseMeta(meta);
        logger.fatal(textLog, meta);
    }

    /**
     * Get the stream of logger to integrate with other logger systems
     * @returns {{write: write}}
     */
    stream () {
        const that = this;
        return {
            write: function (message) {
                that.info(message);
            }
        }
    }

}

/**
 * Static Constructor for the new Logger
 * @param {Object} options Object that represents a prorperties for the new logger
 * @param {String} options.project REQUIRED Project that represents logger
 * @param {String} options.file REQUIRED File of that represents file
 * @returns {LoggerObject} New Logger
 */
function getLogger (options) {
    return new LoggerObject(options);
}

module.exports = getLogger;
