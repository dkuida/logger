'use strict';

const proxyquire = require('proxyquire');
const loggerMock = jasmine.createSpyObj('logger', ['debug', 'fatal', 'info']);
const winstonMock = {
    Logger: function () {
        return loggerMock;
    }
}
const loggerBuilder = proxyquire('../logger', {
    'winston': winstonMock
});
const logger = loggerBuilder({file: 'test', project: 'logger'});


describe('Logger Required', function () {
    beforeEach(() => {
        loggerMock.fatal.calls.reset();
        loggerMock.info.calls.reset();
        loggerMock.debug.calls.reset();
    });
    it('expect logger called', function () {
        /**arrange*/
        /**act*/
        logger.info('HELLO');
        /**assert*/
        expect(loggerMock.info).toHaveBeenCalled();
    });
    it('', function () {
        /**arrange*/
        /**act*/
        logger.fatal('FATAL');
        /**assert*/
        expect(loggerMock.fatal).toHaveBeenCalled();
    });

});
