'use strict';

module.exports = {
    console: {
        level: 'debug'
    },
    file: {
        level: 'debug',
        path: './logs/json-server.log',
        maxSize: 5242880,
        maxFiles: 5
    },
    logstash: {
        level: 'debug',
        nodeName: '',
        host:'',
        port: 0
    }
};
